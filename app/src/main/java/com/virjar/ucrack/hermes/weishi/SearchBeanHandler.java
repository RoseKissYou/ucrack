package com.virjar.ucrack.hermes.weishi;

import com.virjar.hermes.hermesagent.hermes_api.ActionRequestHandler;
import com.virjar.hermes.hermesagent.hermes_api.WrapperAction;
import com.virjar.hermes.hermesagent.hermes_api.aidl.InvokeRequest;
import com.virjar.hermes.hermesagent.hermes_api.aidl.InvokeResult;
import com.virjar.xposed_extention.ClassLoadMonitor;

import org.apache.commons.lang3.StringUtils;

import de.robv.android.xposed.XposedHelpers;

@WrapperAction("search")
public class SearchBeanHandler implements ActionRequestHandler {
    @Override
    public Object handleRequest(InvokeRequest invokeRequest) {
        String key = invokeRequest.getString("key");
        if (StringUtils.isBlank(key)) {
            return InvokeResult.failed("the param {key} not presented");
        }
        String attach_info = invokeRequest.getString("attach_info");
        Object uniqueID = XposedHelpers.callStaticMethod(ClassLoadMonitor.tryLoadClass("com.tencent.ttpic.util.Utils"), "generateUniqueId");
        if (StringUtils.isBlank(attach_info)) {
            attach_info = "";
        }
        return WeishiUtil.sendRequest(XposedHelpers.newInstance(ClassLoadMonitor.tryLoadClass("com.tencent.oscar.module.discovery.ui.adapter.h")
                , uniqueID, key, 0, 0, attach_info));
    }
}
